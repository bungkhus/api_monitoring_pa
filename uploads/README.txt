=================================
	Versioning
=================================

As of VirtualBox 4.0, phpVirtualBox versioning is aligned with VirtualBox versioning in that the major and minor release numbers will maintain compatibility. phpVirtualBox 4.0-x will always be compatible with VirtualBox 4.0.x. Regardless of what the latest x revision is. phpVirtualBox 4.2-x will always be compatible with VirtualBox 4.2.x, etc..

To download phpVirtualBox compatible with previous VirtualBox releases, select Deprecated downloads  on the Downloads tab and click Search.

==================================
	Change Log
==================================

The CHANGELOG.txt download will always contain the Change Log for the most recent version of phpVirtualBox.

==================================
	LATEST.txt
==================================

This is a text file that will always contain a link to latest version of phpVirtualBox. Use to programmatically determine and download the latest version. E.g.

wget `wget -q -O - http://phpvirtualbox.googlecode.com/files/LATEST.txt` -O phpvirtualbox-latest.zip

Will download the latest version of phpVirtualBox to the file phpvirtualbox-latest.zip.