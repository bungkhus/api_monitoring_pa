<?php

class DB_Functions {

    private $db; 
    // constructor 
    function __construct() { 
        // require_once 'DB_Connect.php'; 
        // Koneksi ke database 
        // $this->db = new DB_Connect(); 
        //$this->db->connect(); 
    }

    // destructor 
    function __destruct() { 
        
    } 

    public function conn(){
        require_once 'DB_Connect.php'; 
        $this->db = new DB_Connect(); 
        return $this->db->connect(); 
    }
    
    /** 
     * Cek user di database 
     */ 
    public function getUserByusernameAndPassword($username, $password) { 
        $result = mysqli_query($this->conn(), "SELECT * FROM account WHERE username = '$username' AND status= 'active' ") or die(mysql_error()); 
        $no_of_rows = mysqli_num_rows($result); 
        if ($no_of_rows > 0) { 
            $result = mysqli_fetch_array($result); 
            $encrypted_password = $result['password']; 
            // Cek kesamaan password 
            if ($encrypted_password == md5($password)) { 
                // Identifikasi berhasil 
                $arrData['username']    =   $result['username'];
                $arrData['kode']        =   $result['kode'];
                $arrData['akses']       =   $result['hak_akses'];

                //echo "test"; exit;
                if($arrData['akses'] == "Dosen"){
                    $data = mysqli_query($this->conn(), "SELECT * FROM dosen WHERE kode_dosen = '".$arrData['kode']."' ") or die(mysql_error()); 
                }else if($arrData['akses'] == "Mahasiswa"){
                    $data = mysqli_query($this->conn(), "SELECT * FROM mahasiswa WHERE nim = '".$arrData['kode']."' ") or die(mysql_error()); 
                    $pa = mysqli_query($this->conn(), "SELECT * FROM pa WHERE nim = '".$arrData['kode']."' AND status = 'active' ") or die(mysql_error()); 
                    $pa = mysqli_fetch_array($pa);
                    $arrData['id_pa']    =   $pa['id_pa'];
                    $arrData['judul']    =   $pa['judul'];
                
                    $fungsi = mysqli_query($this->conn(), "SELECT avg(persentase_progress) as progress FROM target_fungsionalitas where id_pa = '".$arrData['id_pa']."'") or die(mysql_error()); 
                    $fungsi = mysqli_fetch_array($fungsi);
                    $laporan = mysqli_query($this->conn(), "SELECT avg(persentase_progress) as progress FROM target_laporan where id_pa = '".$arrData['id_pa']."'") or die(mysql_error()); 
                    $laporan = mysqli_fetch_array($laporan);
                    $arrData['laporan_progress']    =   $laporan['progress'];
                    $arrData['fungsi_progress']    =   $fungsi['progress'];
                }
                $data = mysqli_fetch_array($data);
                $arrData['nama']    =   $data['nama'];
                $arrData['email']    =   $data['email'];
                $arrData['no_tlp']    =   $data['no_tlp'];

                return $arrData; 
            }else{
                return false;
            }
        } else { 
            // User tidak ditemukan 
            return false; 
        } 
    }

    public function getJudulPA($kode_dosen) { 
        $result = mysqli_query($this->conn(), "SELECT t1.id_pa, t1.nim, t3.nama, t1.judul, t2.pembimbing_ke FROM pa as t1 JOIN membimbing_pa as t2 ON t1.id_pa = t2.id_pa JOIN mahasiswa as t3 on t1.nim = t3.nim where t2.kode_dosen = '".$kode_dosen."' AND t1.status = 'active' order by nama ") or die(mysql_error()); 
        $no_of_rows = mysqli_num_rows($result); 
        if ($no_of_rows > 0) { 
            $emparray = array();
            $x = 0;
            while($row =mysqli_fetch_assoc($result))
            {
                $emparray[] = array("id_pa" => $row['id_pa'], "nim" => $row['nim'], "nama" => $row['nama'], "judul" => $row['judul'], "pembimbing_ke" => $row['pembimbing_ke']);
                
                $fungsi = mysqli_query($this->conn(), "SELECT avg(persentase_progress) as progress FROM target_fungsionalitas where id_pa = '".$row['id_pa']."'") or die(mysql_error()); 
                $fungsi = mysqli_fetch_array($fungsi);
                $laporan = mysqli_query($this->conn(), "SELECT avg(persentase_progress) as progress FROM target_laporan where id_pa = '".$row['id_pa']."'") or die(mysql_error()); 
                $laporan = mysqli_fetch_array($laporan);
                array_push ($emparray[$x], $laporan['progress'], $fungsi['progress']);
                $x = $x+1;
            }
            return $emparray; 
        } else { 
            // User tidak ditemukan 
            return false; 
        } 
    }

    public function searchJudulPA($kode_dosen, $query) { 
        $result = mysqli_query($this->conn(), "SELECT t1.id_pa, t1.nim, t3.nama, t1.judul, t2.pembimbing_ke FROM pa as t1 JOIN membimbing_pa as t2 ON t1.id_pa = t2.id_pa JOIN mahasiswa as t3 on t1.nim = t3.nim where t2.kode_dosen = '".$kode_dosen."' AND t1.status = 'active' AND t3.nama like '%".$query."%' order by nama ") or die(mysql_error()); 
        $no_of_rows = mysqli_num_rows($result); 
        if ($no_of_rows > 0) { 
            $emparray = array();
            $x = 0;
            while($row =mysqli_fetch_assoc($result))
            {
                $emparray[] = array("id_pa" => $row['id_pa'], "nim" => $row['nim'], "nama" => $row['nama'], "judul" => $row['judul'], "pembimbing_ke" => $row['pembimbing_ke']);
                
                $fungsi = mysqli_query($this->conn(), "SELECT avg(persentase_progress) as progress FROM target_fungsionalitas where id_pa = '".$row['id_pa']."'") or die(mysql_error()); 
                $fungsi = mysqli_fetch_array($fungsi);
                $laporan = mysqli_query($this->conn(), "SELECT avg(persentase_progress) as progress FROM target_laporan where id_pa = '".$row['id_pa']."'") or die(mysql_error()); 
                $laporan = mysqli_fetch_array($laporan);
                array_push ($emparray[$x], $laporan['progress'], $fungsi['progress']);
                $x = $x+1;
            }
            return $emparray; 
        } else { 
            // User tidak ditemukan 
            return false; 
        } 
    }

    public function getPengerjaanFungsionalitas($id_fungsi) { 
        $result = mysqli_query($this->conn(), "SELECT * FROM pengerjaan_fungsi WHERE id_fungsionalitas = '$id_fungsi' ") or die(mysql_error()); 
        $no_of_rows = mysqli_num_rows($result); 
        if ($no_of_rows > 0) { 
            $emparray = array();
            while($row =mysqli_fetch_assoc($result))
            {
                $emparray[] = $row;
            }
            return $emparray; 
        } else { 
            // User tidak ditemukan 
            return false; 
        } 
    }

    public function getPengerjaanFungsi($id_fungsi) { 
        $result = mysqli_query($this->conn(), "SELECT * FROM pengerjaan_fungsi WHERE id_pengerjaan_fungsi = '$id_fungsi' ") or die(mysql_error()); 
        $no_of_rows = mysqli_num_rows($result); 
        if ($no_of_rows > 0) { 
            $emparray = array();
            while($row =mysqli_fetch_assoc($result))
            {
                $emparray[] = $row;
            }
            return $emparray; 
        } else { 
            // User tidak ditemukan 
            return false; 
        } 
    }

    public function getPengerjaanLaporan($id_fungsi) { 
        $result = mysqli_query($this->conn(), "SELECT * FROM pengerjaan_laporan WHERE id_laporan = '$id_fungsi' ") or die(mysql_error()); 
        $no_of_rows = mysqli_num_rows($result); 
        if ($no_of_rows > 0) { 
            $emparray = array();
            while($row =mysqli_fetch_assoc($result))
            {
                $emparray[] = $row;
            }
            return $emparray; 
        } else { 
            // User tidak ditemukan 
            return false; 
        } 
    }

    public function getPengerjaanPerLaporan($id_fungsi) { 
        $result = mysqli_query($this->conn(), "SELECT * FROM pengerjaan_laporan WHERE id_pengerjaan_laporan = '$id_fungsi' ") or die(mysql_error()); 
        $no_of_rows = mysqli_num_rows($result); 
        if ($no_of_rows > 0) { 
            $emparray = array();
            while($row =mysqli_fetch_assoc($result))
            {
                $emparray[] = $row;
            }
            return $emparray; 
        } else { 
            // User tidak ditemukan 
            return false; 
        } 
    }

    public function getFeedbackFungsi($id_pengerjaan) { 
        $result = mysqli_query($this->conn(), "SELECT * FROM feedback_fungsi WHERE id_pengerjaan_fungsi = '$id_pengerjaan' ") or die(mysql_error()); 
        $no_of_rows = mysqli_num_rows($result); 
        if ($no_of_rows > 0) { 
            $emparray = array();
            while($row =mysqli_fetch_assoc($result))
            {
                $emparray[] = $row;
            }
            return $emparray; 
        } else { 
            // User tidak ditemukan 
            return false; 
        } 
    }

    public function getFungsionalitas($id_pa) { 
        $result = mysqli_query($this->conn(), "SELECT * FROM target_fungsionalitas WHERE id_pa = '$id_pa' ") or die(mysql_error()); 
        $no_of_rows = mysqli_num_rows($result); 
        if ($no_of_rows > 0) { 
            $emparray = array();
            while($row =mysqli_fetch_assoc($result))
            {
                $emparray[] = $row;
            }
            return $emparray; 
        } else { 
            // User tidak ditemukan 
            return false; 
        } 
    }

    public function getLaporan($id_pa) { 
        $result = mysqli_query($this->conn(), "SELECT t1.*, t2.nama_bab FROM target_laporan as t1 JOIN bab_laporan as t2 ON t2.id_bab = t1.id_bab WHERE t1.id_pa = '$id_pa' ") or die(mysql_error()); 
        $no_of_rows = mysqli_num_rows($result); 
        if ($no_of_rows > 0) { 
            $emparray = array();
            while($row =mysqli_fetch_assoc($result))
            {
                $emparray[] = $row;
            }
            return $emparray; 
        } else { 
            // User tidak ditemukan 
            return false; 
        } 
    }

    public function setApprovedLaporan($id, $kode) { 
        $result = mysqli_query($this->conn(), "UPDATE target_laporan SET status_approve = '".$kode."' WHERE id_laporan = '".$id."'") or die(mysql_error()); 
        return $result;
    }

    public function setTargetLaporan($id, $tgl, $bobot, $kode) { 
        $result = mysqli_query($this->conn(), "UPDATE target_laporan SET tgl_selesai = '".$tgl."', bobot = '".$bobot."', status_approve = '".$kode."' WHERE id_laporan = '".$id."'") or die(mysql_error()); 
        return $result;
    }

    public function setApprovedAplikasi($id, $kode) { 
        $result = mysqli_query($this->conn(), "UPDATE target_fungsionalitas SET status_approve = '".$kode."' WHERE id_laporan = '".$id."'") or die(mysql_error()); 
        return $result;
    }

    public function setTargetAplikasi($id, $tgl, $bobot, $kode) { 
        $result = mysqli_query($this->conn(), "UPDATE target_fungsionalitas SET tgl_selesai = '".$tgl."', bobot = '".$bobot."', status_approve = '".$kode."' WHERE id_fungsionalitas = '".$id."'") or die(mysql_error()); 
        return $result;
    }

    public function setKontrakSeminar($kontrak, $id_pa) { 
        $result = mysqli_query($this->conn(), "INSERT INTO target_fungsionalitas (id_fungsionalitas, id_pa, nama_fungsionalitas, deskripsi_fungsionalitas, bobot, persentase_progress)
                                                values ('', '".$id_pa."', '".$kontrak."', '<p>".$kontrak."</p>', '0', '0')") or die(mysql_error()); 
        return $result;
    }

    public function setLaporan($id_pa, $jumlah) { 

        $result = "0";

        for ($x = $jumlah+1; $x <= 5; $x++) {
            $result = mysqli_query($this->conn(), "INSERT INTO target_laporan (id_laporan, id_pa, id_bab, tgl_selesai, bobot, status_approve, keterangan_revisi, persentase_progress) 
                values ('', '".$id_pa."', '".$x."', now(),'0', '', '', '0')") or die(mysql_error()); 
        }

        return $result;
    }

    public function deleteKontrakSeminar($nama_fungsionalitas, $id_pa) { 
        $result = mysqli_query($this->conn(), "DELETE FROM target_fungsionalitas WHERE nama_fungsionalitas = '".$nama_fungsionalitas."' AND id_pa = '".$id_pa."'") or die(mysql_error()); 
        return $result;
    }

    /** 
     * Cek user exist atau tidak 
     */ 
    public function isUserExisted($username) { 
        $result = mysqli_query($this->conn(), "SELECT * from account WHERE username = '$username'"); 
        $no_of_rows = mysql_num_rows($result); 
        if ($no_of_rows > 0) { 
            // user existed 
            return true; 
        } else { 
            // user not existed 
            return false; 
        } 
    }
} 
?>