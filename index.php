<?php

/*
File ini untuk menghandle API Request 
hasilnya di encoda dalam bentuk JSON 
  /*
* check for POST request 
*/ 

    // Database Handler 
require_once 'DB_Function.php'; 
$db = new DB_Functions();

if (isset($_GET['tag']) && $_GET['tag'] != '') { 
    // Tag 
    $tag = $_GET['tag'];

    // Response--> data yang akan dikembalikan 
    $response = array("tag" => $tag, "success" => 0); 
    
    // Cek tipe tag 
    if ($tag == 'login') { 
        // Tangkap data yang dikirim dari android 
        $username = $_GET['username']; 
        $password = $_GET['password'];

        // Cek user 
        $user = $db->getUserByusernameAndPassword($username, $password); 
        if ($user != false) { 
            // User ditemukan 
            $response["success"] = $user; 
            echo json_encode($response); 
        } else { 
            // User tidak ditemukan 
            $response["success"] = 0; 
            echo json_encode($response); 
        } 
    } else if ($tag == 'get_judul_pa') { 
        // Tangkap data yang dikirim dari android 
        $id_pa = $_GET['kode_dosen']; 

        // Cek user 
        $getJudulPA = $db->getJudulPA($id_pa); 
        if ($getJudulPA != false) { 
            // User ditemukan 
            $response["success"] = $getJudulPA; 
            echo json_encode($response); 
        } else { 
            // User tidak ditemukan 
            $response["success"] = 'Empty'; 
            echo json_encode($response); 
        } 
    } else if ($tag == 'search_judul_pa') { 
        // Tangkap data yang dikirim dari android 
        $id_pa = $_GET['kode_dosen']; 
        $query = $_GET['query']; 

        // Cek user 
        $searchJudulPA = $db->searchJudulPA($id_pa, $query); 
        if ($searchJudulPA != false) { 
            // User ditemukan 
            $response["success"] = $searchJudulPA; 
            echo json_encode($response); 
        } else { 
            // User tidak ditemukan 
            $response["success"] = 'Empty'; 
            echo json_encode($response); 
        } 
    } else if ($tag == 'get_pengerjaan_fungsionalitas') { 
        // Tangkap data yang dikirim dari android 
        $id_pa = $_GET['id_fungsi']; 

        // Cek user 
        $getPengerjaanFungsionalitas = $db->getPengerjaanFungsionalitas($id_pa); 
        if ($getPengerjaanFungsionalitas != false) { 
            // User ditemukan 
            $response["success"] = $getPengerjaanFungsionalitas; 
            echo json_encode($response); 
        } else { 
            // User tidak ditemukan 
            $response["success"] = 'Empty'; 
            echo json_encode($response); 
        } 
    } else if ($tag == 'get_pengerjaan_fungsi') { 
        // Tangkap data yang dikirim dari android 
        $id_pa = $_GET['id_fungsi']; 

        // Cek user 
        $getPengerjaanFungsi = $db->getPengerjaanFungsi($id_pa); 
        if ($getPengerjaanFungsi != false) { 
            // User ditemukan 
            $response["success"] = $getPengerjaanFungsi; 
            echo json_encode($response); 
        } else { 
            // User tidak ditemukan 
            $response["success"] = 'Empty'; 
            echo json_encode($response); 
        } 
    } else if ($tag == 'get_pengerjaan_laporan') { 
        // Tangkap data yang dikirim dari android 
        $id_pa = $_GET['id_fungsi']; 

        // Cek user 
        $getPengerjaanLaporan = $db->getPengerjaanLaporan($id_pa); 
        if ($getPengerjaanLaporan != false) { 
            // User ditemukan 
            $response["success"] = $getPengerjaanLaporan; 
            echo json_encode($response); 
        } else { 
            // User tidak ditemukan 
            $response["success"] = 'Empty'; 
            echo json_encode($response); 
        } 
    } else if ($tag == 'get_pengerjaan_per_laporan') { 
        // Tangkap data yang dikirim dari android 
        $id_pa = $_GET['id_fungsi']; 

        // Cek user 
        $getPengerjaanPerLaporan = $db->getPengerjaanPerLaporan($id_pa); 
        if ($getPengerjaanPerLaporan != false) { 
            // User ditemukan 
            $response["success"] = $getPengerjaanPerLaporan; 
            echo json_encode($response); 
        } else { 
            // User tidak ditemukan 
            $response["success"] = 'Empty'; 
            echo json_encode($response); 
        } 
    } else if ($tag == 'get_feedback_fungsi') { 
        // Tangkap data yang dikirim dari android 
        $id_pa = $_GET['id_pengerjaan']; 

        // Cek user 
        $getFeedbackFungsi = $db->getFeedbackFungsi($id_pa); 
        if ($getFeedbackFungsi != false) { 
            // User ditemukan 
            $response["success"] = $getFeedbackFungsi; 
            echo json_encode($response); 
        } else { 
            // User tidak ditemukan 
            $response["success"] = 'Empty'; 
            echo json_encode($response); 
        } 
    } else if ($tag == 'get_fungsionalitas') { 
        // Tangkap data yang dikirim dari android 
        $id_pa = $_GET['id_pa']; 

        // Cek user 
        $getFungsionalitas = $db->getFungsionalitas($id_pa); 
        if ($getFungsionalitas != false) { 
            // User ditemukan 
            $response["success"] = $getFungsionalitas; 
            echo json_encode($response); 
        } else { 
            // User tidak ditemukan 
            $response["success"] = 'Empty'; 
            echo json_encode($response); 
        } 
    } else if ($tag == 'get_laporan') { 
        // Tangkap data yang dikirim dari android 
        $id_pa = $_GET['id_pa']; 

        // Cek user 
        $getLaporan = $db->getLaporan($id_pa); 
        if ($getLaporan != false) { 
            // User ditemukan 
            $response["success"] = $getLaporan; 
            echo json_encode($response); 
        } else { 
            // User tidak ditemukan 
            $response["success"] = 'Empty'; 
            echo json_encode($response); 
        } 
    } else if ($tag == 'get_delete_kontrak_seminar') { 
        $deleteKontrakSeminar = $db->deleteKontrakSeminar($_GET['nama_fungsionalitas'], $_GET['id_pa']);
        if ($deleteKontrakSeminar != false) { 
            // User ditemukan 
            $response["success"] = 1; 
            echo json_encode($response); 
        } else { 
            // User tidak ditemukan 
            $response["success"] = 0; 
            echo json_encode($response); 
        } 
    } else { 
        echo "Invalid Request"; 
    } 
}else if(isset($_POST['tag']) && $_POST['tag'] != ''){
    $tag = $_POST['tag'];
    if ($tag == 'post_kontrak_seminar') { 
        $setKontrakSeminar = $db->setKontrakSeminar($_POST['isi_kontrak_seminar'], $_POST['id_pa']);
        if ($setKontrakSeminar != false) { 
            // User ditemukan 
            $response["success"] = 1; 
            echo json_encode($response); 
        } else { 
            // User tidak ditemukan 
            $response["success"] = 0; 
            echo json_encode($response); 
        } 
    } else if ($tag == 'delete_kontrak_seminar') { 
        $deleteKontrakSeminar = $db->deleteKontrakSeminar($_POST['nama_fungsionalitas'], $_POST['id_pa']);
        if ($deleteKontrakSeminar != false) { 
            // User ditemukan 
            $response["success"] = 1; 
            echo json_encode($response); 
        } else { 
            // User tidak ditemukan 
            $response["success"] = 0; 
            echo json_encode($response); 
        } 
    } else if ($tag == 'post_laporan') { 
        $setLaporan = $db->setLaporan($_POST['id_pa'], $_POST['jumlah']);
        if ($setLaporan != false) { 
            // User ditemukan 
            $response["success"] = 1; 
            echo json_encode($response); 
        } else { 
            // User tidak ditemukan 
            $response["success"] = 0; 
            echo json_encode($response); 
        } 
    } else if ($tag == 'post_target_laporan') { 
        $setTargetLaporan = $db->setTargetLaporan($_POST['id'], $_POST['tgl'], $_POST['bobot'], $_POST['kode']);
        if ($setTargetLaporan != false) { 
            // User ditemukan 
            $response["success"] = 1; 
            echo json_encode($response); 
        } else { 
            // User tidak ditemukan 
            $response["success"] = 0; 
            echo json_encode($response); 
        } 
    } else if ($tag == 'post_target_aplikasi') { 
        $setTargetAplikasi = $db->setTargetAplikasi($_POST['id'], $_POST['tgl'], $_POST['bobot'], $_POST['kode']);
        if ($setTargetAplikasi != false) { 
            // User ditemukan 
            $response["success"] = 1; 
            echo json_encode($response); 
        } else { 
            // User tidak ditemukan 
            $response["success"] = 0; 
            echo json_encode($response); 
        } 
    } else if ($tag == 'post_approve_target_laporan') { 
        $setApprovedLaporan = $db->setApprovedLaporan($_POST['id'], $_POST['kode']);
        if ($setApprovedLaporan != false) { 
            // User ditemukan 
            $response["success"] = 1; 
            echo json_encode($response); 
        } else { 
            // User tidak ditemukan 
            $response["success"] = 0; 
            echo json_encode($response); 
        } 
    } else if ($tag == 'post_approve_target_aplikasi') { 
        $setApprovedAplikasi = $db->setApprovedAplikasi($_POST['id'], $_POST['kode']);
        if ($setApprovedAplikasi != false) { 
            // User ditemukan 
            $response["success"] = 1; 
            echo json_encode($response); 
        } else { 
            // User tidak ditemukan 
            $response["success"] = 0; 
            echo json_encode($response); 
        } 
    } else { 
        echo "Invalid Request"; 
    } 
} else { 
    echo "Access Denied"; 
} 
?>